# flutter-app

notre application a pour objectif de faire le calcul du BMI (indice de masse corporelle)pour
un utilisateur inscrit apres avoir entré son poids et sa taille et de lui indiquer ainsi son
état corporel et comment il peut preserver son bien etre à travers des conseils concernant
l'alimentation et des routines sportives proposées soient qu'il exerce chez lui ou sous la
suivi d'un coach selon son choix.
pour cela, nous avons réalisé 9 interfaces:
interface1:c'est l'interface du logo.
interface2:c'est l'interface de connexion pou les utilisateurs déjà inscrits.
interface3:c'est l'interface de l'inscription pour les nouveaux utilisateurs.
interface4:l'utilisateur doit indiquer son age, son poids et sa taille pour le calcul de son BMI.
interface5:c'est où il y aura l'affichage du résultat acoompagné d'une remaque concernant l'état
  corporel ainsi que le routage vers l'interface nutrition et le choix du rythme des exercices.
interface6: c'est où l'utilisateur peut avoir une idée sur le type d'aliments à consommer ainsi
   qu'à éviter.
interface7:c'est l'ensemble d'exercices que l'utilisateur doit exercer
interface8:c'est la liste de coachs inscrits au programme.
interface9:c'est où l'utilisateur peut évaluer la pertinence et l'efficacité de l'application.